FROM node:alpine AS build

WORKDIR /opt

RUN npm install yarn

COPY .nvmrc package.json yarn.lock ./

RUN yarn install

COPY . ./

ARG API_HOST
ARG API_PORT

ENV API_HOST=${API_HOST}
ENV API_PORT=${API_PORT}

RUN mv src/environments/environment.docker.ts src/environments/environment.prod.ts

RUN npm run build

FROM nginx

COPY default.conf /etc/nginx/config.d/default.conf
COPY default.conf /etc/nginx/conf.d/default.conf

COPY --from=build /opt/dist /usr/share/nginx/html

RUN  ["rm", "-rf", "/etc/localtime"]

RUN  ["ln", "-s", "/usr/share/zoneinfo/Asia/Ho_Chi_Minh", "/etc/localtime"]

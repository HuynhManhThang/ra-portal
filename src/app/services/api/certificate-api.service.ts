import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { QueryFilerModel } from '@model';
import { CertificateRouter } from '@util';
// RxJS
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CertificateApiService {
  constructor(private http: _HttpClient) {}

  create(model: any): Observable<any> {
    return this.http.post(environment.BASE_API_URL + CertificateRouter.create, model);
  }

  // update(model: any): Observable<any> {
  //   return this.http.put(environment.BASE_API_URL + AgencyRouter.update, model);
  // }

  getById(id: string): Observable<any> {
    return this.http.get(environment.BASE_API_URL + CertificateRouter.getById + id);
  }

  // delete(list: string[]): Observable<any> {
  //   const option = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //     }),
  //     body: list,
  //   };
  //   return this.http.request('delete', environment.BASE_API_URL + AgencyRouter.delete, option);
  // }

  getFilter(model: QueryFilerModel): Observable<any> {
    return this.http.post(environment.BASE_API_URL + CertificateRouter.getFilter, model);
  }

  getAll(): Observable<any> {
    return this.http.get(environment.BASE_API_URL + CertificateRouter.getALL + `?cn=0`);
  }

  // getListCombobox(): Observable<any> {
  //   return this.http.get(environment.BASE_API_URL + AgencyRouter.getListCombobox);
  // }
}

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core"
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StepService {
  constructor(
  ) {
  }
  public step = new BehaviorSubject('');
  getStep = this.step.asObservable();
  setStep(item: any) {
    this.step.next(item)
  }
}

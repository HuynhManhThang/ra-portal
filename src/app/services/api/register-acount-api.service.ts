import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { QueryFilerModel } from '@model';
import { RegisterAccountRouter } from '@util';
// RxJS
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RegisterAccountService {
  constructor(private http: _HttpClient) { }

  create(model: any): Observable<any> {
    return this.http.post(environment.BASE_API_URL + RegisterAccountRouter.create, model);
  }

  getById(id: string): Observable<any> {
    return this.http.get(environment.BASE_API_URL + RegisterAccountRouter.getById + id);
  }

}

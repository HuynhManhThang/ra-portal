import { TestBed } from '@angular/core/testing';

import { Wso2Service } from './wso2.service';

describe('Wso2Service', () => {
  let service: Wso2Service;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Wso2Service);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

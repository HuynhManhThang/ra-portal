import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { environment } from '@env/environment';
import { authWSO2 } from '@util';

@Component({
  selector: '[layout-pro-header-widget]',
  templateUrl: './widget.component.html',
  host: {
    '[class.alain-pro__header-right]': 'true',
  },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProHeaderWidgetComponent {
  constructor(private router: Router, @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) {}
  logout(): void {
    const authType = environment.AUTH_TYPE;
    let data_token;
    switch (authType) {
      case 'jwt':
        this.tokenService.clear();
        this.router.navigateByUrl('passport/login');
        break;

      case 'wso2':
        data_token = JSON.parse(localStorage.getItem('data_token') || '{}');
        if (data_token !== {} && data_token !== null) {
          const url =
            environment.BASE_WSO2_URL +
            authWSO2.logoutWso2 +
            data_token.id_token +
            authWSO2.post_logout_redirect_uri +
            environment.BASE_LOGOUT_URL;
          window.location.href = url;
          this.tokenService.clear();
        }
        break;
      default:
        data_token = JSON.parse(localStorage.getItem('data_token') || '{}');
        if (data_token !== {} && data_token !== null) {
          const url =
            environment.BASE_WSO2_URL +
            authWSO2.logoutWso2 +
            data_token.id_token +
            authWSO2.post_logout_redirect_uri +
            environment.BASE_LOGOUT_URL;
          window.location.href = url;
          this.tokenService.clear();
        }
        break;
    }
  }
}

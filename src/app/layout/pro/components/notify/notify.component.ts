import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { NoticeIconList, NoticeItem } from '@delon/abc/notice-icon';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import parse from 'date-fns/parse';
import { NzSafeAny } from 'ng-zorro-antd/core/types';
import { NzI18nService } from 'ng-zorro-antd/i18n';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'layout-pro-notify',
  templateUrl: './notify.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetNotifyComponent {
  data: NoticeItem[] = [
    {
      title: 'Thông báo',
      list: [],
      emptyText: 'Không có Thông báo',
      emptyImage: 'https://gw.alipayobjects.com/zos/rmsportal/wAhyIChODzsoKIOBHcBk.svg',
      clearText: 'Xem tất cả',
    }
  ];
  count = 5;
  loading = false;

  constructor(private msg: NzMessageService, private nzI18n: NzI18nService, private cdr: ChangeDetectorRef) { }

  updateNoticeData(notices: NoticeIconList[]): NoticeItem[] {
    const data = this.data.slice();
    data.forEach((i) => (i.list = []));

    notices.forEach((item) => {
      const newItem = { ...item };
      if (typeof newItem.datetime === 'string') {
        newItem.datetime = parse(newItem.datetime, 'yyyy-MM-dd', new Date());
      }
      if (newItem.datetime) {
        newItem.datetime = formatDistanceToNow(newItem.datetime as Date, { locale: this.nzI18n.getDateLocale() });
      }
      if (newItem.extra && newItem.status) {
        newItem.color = ({
          todo: undefined,
          processing: 'blue',
          urgent: 'red',
          doing: 'gold',
        } as NzSafeAny)[newItem.status];
      }
      data.find((w) => w.title === newItem.type)?.list.push(newItem);
    });
    return data;
  }

  loadData(): void {
    if (this.loading) {
      return;
    }
    this.loading = true;
    setTimeout(() => {
      this.data = this.updateNoticeData([
        {
          id: '000000001',
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png',
          title: 'Yêu cầu đăng ký chứng thư số đã được duyệt, bạn có thể thực hiện thanh toán!',
          datetime: '2017-08-09',
          type: 'Thông báo',
        },
        {
          id: '000000002',
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/OKJXDXrmkNshAMvwtvhu.png',
          title: 'Yêu cầu đăng ký chứng thư số đã được duyệt, bạn có thể thực hiện thanh toán!',
          datetime: '2017-08-08',
          type: 'Thông báo',
        },
        {
          id: '000000003',
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/kISTdvpyTAhtGxpovNWd.png',
          title: 'Yêu cầu đăng ký chứng thư số đã được duyệt, bạn có thể thực hiện thanh toán!',
          datetime: '2017-08-07',
          read: true,
          type: 'Thông báo',
        },
        {
          id: '000000004',
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/GvqBnKhFgObvnSGkDsje.png',
          title: 'Yêu cầu đăng ký chứng thư số đã được duyệt, bạn có thể thực hiện thanh toán!',
          datetime: '2017-08-07',
          type: 'Thông báo',
        },
        {
          id: '000000005',
          avatar: 'https://gw.alipayobjects.com/zos/rmsportal/ThXAXghbEsBCCSDihZxY.png',
          title: 'Yêu cầu đăng ký chứng thư số đã được duyệt, bạn có thể thực hiện thanh toán!',
          datetime: '2017-08-07',
          type: 'Thông báo',
        }
      ]);

      this.loading = false;

      this.cdr.detectChanges();
    }, 1000);
  }

  clear(type: string): void {
    this.msg.success(`清空了 ${type}`);
  }

  select(res: any): void {
    this.msg.success(`点击了 ${res.title} 的 ${res.item.title}`);
  }
}

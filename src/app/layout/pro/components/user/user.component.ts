import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { SettingsService } from '@delon/theme';
import { environment } from '@env/environment';
import { authWSO2 } from '@util';

@Component({
  selector: 'layout-pro-user',
  templateUrl: 'user.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutProWidgetUserComponent implements OnInit {
  constructor(public settings: SettingsService, private router: Router, @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService) {}
  userInfo: any;
  ngOnInit(): void {
    // mock
    const token = this.tokenService.get() || {
      token: 'nothing',
      name: 'Admin',
      avatar: './assets/logo-color.svg',
      email: 'cipchk@qq.com',
    };
    this.userInfo = token;
    this.tokenService.set(token);
  }

  logout(): void {
    let data_token;
    const authType = environment.AUTH_TYPE;
    switch (authType) {
      case 'jwt':
        this.tokenService.clear();
        this.router.navigateByUrl('passport/login');
        break;

      case 'wso2':
        data_token = JSON.parse(localStorage.getItem('data_token') || '{}');
        if (data_token !== {} && data_token !== null) {
          const url =
            environment.BASE_WSO2_URL +
            authWSO2.logoutWso2 +
            data_token.id_token +
            authWSO2.post_logout_redirect_uri +
            environment.BASE_LOGOUT_URL;
          window.location.href = url;
          this.tokenService.clear();
        }
        break;
      default:
        data_token = JSON.parse(localStorage.getItem('data_token') || '{}');
        if (data_token !== {} && data_token !== null) {
          const url =
            environment.BASE_WSO2_URL +
            authWSO2.logoutWso2 +
            data_token.id_token +
            authWSO2.post_logout_redirect_uri +
            environment.BASE_LOGOUT_URL;
          window.location.href = url;
          this.tokenService.clear();
        }
        break;
    }
  }
}

import { Component, Inject, OnInit } from '@angular/core';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { SettingsService } from '@delon/theme';

@Component({
  selector: 'layout-passport',
  templateUrl: './passport.component.html',
  styleUrls: ['./passport.component.less'],
})
export class LayoutPassportComponent implements OnInit {
  links = [];
  app: any;

  constructor(@Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService, private settingService: SettingsService) {
    this.app = this.settingService.getData('app');
  }

  ngOnInit(): void {
    this.tokenService.clear();
  }
}

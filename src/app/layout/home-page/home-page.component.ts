import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styles: [`
  :host {
    text-align: center;
  }
  .logo {
    float: left;
    width: 120px;
    font-size: 28px;
    font-weight: 500;
    color: indianred;
    white-space: nowrap;
  }
  nz-header{
    background: #fff;
    margin-bottom:24px
  },
  nz-layout{
    min-height:74vh
  }

  nz-footer {
    line-height: 1.5;
  }
.menu{
 display: none
}
.header-contet{
  display:block
}
  @media only screen and (max-width : 1192px) {
.menu{
  display:block
}
.header-contet{
  display:none
}
}
  `]
})
export class HomePageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  visible = false;

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }
}

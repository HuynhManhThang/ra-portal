import { Component, ElementRef, Inject, OnInit, Renderer2 } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TitleService, VERSION as VERSION_ALAIN } from '@delon/theme';
import { NzModalService } from 'ng-zorro-antd/modal';
import { VERSION as VERSION_ZORRO } from 'ng-zorro-antd/version';
import { filter } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import 'ag-grid-enterprise';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { Wso2Service } from '@service';

@Component({
  selector: 'app-root',
  template: `
    <app-loader></app-loader>
    <app-messages></app-messages>
    <div style="height: 100%;">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent implements OnInit {
  constructor(
    el: ElementRef,
    renderer: Renderer2,
    private router: Router,
    private notification: NzNotificationService,
    private wso2Service: Wso2Service,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private titleSrv: TitleService,
    private modalSrv: NzModalService,
  ) {
    renderer.setAttribute(el.nativeElement, 'ng-alain-version', VERSION_ALAIN.full);
    renderer.setAttribute(el.nativeElement, 'ng-zorro-version', VERSION_ZORRO.full);
  }

  ngOnInit(): void {
    this.router.events.pipe(filter((evt) => evt instanceof NavigationEnd)).subscribe(() => {
      this.titleSrv.setTitle();
      this.modalSrv.closeAll();
    });
  }
}

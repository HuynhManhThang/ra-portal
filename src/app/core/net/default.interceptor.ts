import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponseBase } from '@angular/common/http';
import { Inject, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { SettingsService, _HttpClient } from '@delon/theme';
import { environment } from '@env/environment';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { BehaviorSubject, Observable, of, throwError } from 'rxjs';
import { catchError, filter, finalize, mergeMap, switchMap, take, tap } from 'rxjs/operators';

import { LoaderService } from '../../services/core/loader.service';
import { MessageService } from '../../services/core/message.service';

const CODEMESSAGE: { [key: number]: string } = {
  200: '200 - Success',
  201: '201 - Success',
  202: '202 - Success',
  204: '204 - Xóa dữ liệu thành công',
  400: '400 - Error',
  401: '401 - Unauthorized',
  403: '403 - Forbidden Error',
  404: '404 - Not Found',
  406: '406 - Not Acceptable',
  409: '409 - Conflict',
  410: '410 - Gone',
  422: '422 - Unprocessable Entity',
  500: '500 - Internal Server Error',
  502: '502 - Bad Gateway',
  503: '503 - Service Unavailable',
  504: '504 - Gateway Timeout',
};

/**
 * The default HTTP interceptor, see the registration details `app.module.ts`
 */
@Injectable()
export class DefaultInterceptor implements HttpInterceptor {
  private refreshTokenType: 're-request' | 'auth-refresh' = 'auth-refresh';
  private refreshToking = false;
  private refreshToken$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private requests: HttpRequest<any>[] = [];

  constructor(
    private injector: Injector,
    private settingsService: SettingsService,
    public loaderService: LoaderService,
    public messageService: MessageService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {
    // if (this.refreshTokenType === 'auth-refresh') {
    //   this.buildAuthRefresh();
    // }
  }

  private get notification(): NzNotificationService {
    return this.injector.get(NzNotificationService);
  }

  private get tokenSrv(): ITokenService {
    return this.injector.get(DA_SERVICE_TOKEN);
  }

  private get http(): _HttpClient {
    return this.injector.get(_HttpClient);
  }

  private goTo(url: string): void {
    setTimeout(() => this.injector.get(Router).navigateByUrl(url));
  }

  private checkStatus(ev: HttpResponseBase): void {
    if ((ev.status >= 200 && ev.status < 300) || ev.status === 401) {
      return;
    }

    const errortext = CODEMESSAGE[ev.status] || ev.statusText;
    // this.notification.error(`Request error ${ev.status}: ${ev.url}`, errortext);
    const mess = `Request error ${ev.status}: ${ev.url}` + errortext;
    // this.messageService.add(mess);
  }

  private toLogin(): void {
    let code;
    const authType = environment.AUTH_TYPE;
    switch (authType) {
      case 'jwt':
        this.goTo('passport/login');
        break;

      case 'wso2':
        code = localStorage.getItem('code');
        // this.notification.error(`Chưa đăng nhập hoặc phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại.`, ``);
        if (code !== null && code !== undefined && code !== '') {
          this.goTo('/confirm?code=' + code);
        }
        break;
      default:
        code = localStorage.getItem('code');
        // this.notification.error(`Chưa đăng nhập hoặc phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại.`, ``);
        if (code !== null && code !== undefined && code !== '') {
          this.goTo('/confirm?code=' + code);
        }
        break;
    }
  }

  private delayGotoLogin(): void {
    setTimeout(() => {
      // Clear token information
      this.settingsService.setUser({});
      (this.injector.get(DA_SERVICE_TOKEN) as ITokenService).clear();
      this.toLogin();
    }, 3000);
  }

  private handleData(ev: HttpResponseBase, req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    this.checkStatus(ev);
    // Business processing: some common operations
    switch (ev.status) {
      case 200:
        break;
      case 400:
        break;
      case 401:
        if (this.tokenService.get()?.token) {
          this.messageService.add(`Not logged in or the login has expired, please log in again`);
        }
        // // Clear token information
        // this.settingsService.setUser({});
        // (this.injector.get(DA_SERVICE_TOKEN) as ITokenService).clear();
        // this.toLogin();
        this.delayGotoLogin();
        break;
      case 403:
      case 404:
      case 409:
      case 500:
        // this.goTo(`/exception/${ev.status}`);
        break;
      default:
        if (ev instanceof HttpErrorResponse) {
          console.warn('Unknown error, mostly due to backend not supporting CORS or invalid configuration', ev);
        }
        break;
    }
    if (ev instanceof HttpErrorResponse) {
      return throwError(ev);
    } else {
      return of(ev);
    }
  }

  removeRequest(req: HttpRequest<any>): any {
    const i = this.requests.indexOf(req);
    if (i >= 0) {
      this.requests.splice(i, 1);
    }
    this.loaderService.isLoading.next(this.requests.length > 0);
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Unified plus server prefix
    let url = req.url;
    if (!url.startsWith('https://') && !url.startsWith('http://')) {
      url = environment.SERVER_URL + url;
    }

    let newReq = req.clone({ url });

    newReq = newReq.clone({
      headers: newReq.headers.set('apikey', environment.API_M_KEY),
    });

    if (this.tokenService.get()?.token) {
      newReq = newReq.clone({
        headers: newReq.headers
          // .set('Authorization', 'Bearer ' + this.tokenService.get()?.token)
          .set('X-ApplicationId', this.tokenService.get()?.appId),
      });
    }

    // Loader icon
    this.requests.push(newReq);
    // console.log('No of requests--->' + this.requests.length);
    this.loaderService.isLoading.next(true);

    return next.handle(newReq).pipe(
      mergeMap((ev) => {
        // Allow unified handling of request errors
        if (ev instanceof HttpResponseBase) {
          return this.handleData(ev, newReq, next);
        }
        // If everything is normal, follow up operations
        return of(ev);
      }),
      catchError((err: HttpErrorResponse) => this.handleData(err, newReq, next)),
      finalize(() => {
        this.removeRequest(newReq);
      }),
    );
  }
}

// Lưu các tham số dùng chung
import { environment } from '@env/environment';
import { QueryFilerModel } from '@model';
import { authWSO2 } from './api-router';

export const ROLE_SYS_ADMIN = 'SYS_ADMIN';

export const LIST_STATUS = [
  { id: true, code: true, name: 'Đang hoạt động' },
  { id: false, code: false, name: 'Ngừng hoạt động' },
];
export const LIST_STATUS_CERT = [
  { id: true, code: true, name: 'Đã cấp phát' },
  { id: false, code: false, name: 'Chưa cấp phát' },
];
export const LIST_CERT_TYPE = [
  { value: 1, label: 'Cá nhân' },
  { value: 2, label: 'Cá nhân thuộc tổ chức' },
  { value: 3, label: 'Tổ chức' },
];
export const LIST_IDENTITY_NUMBER_TYPE = [
  { value: 'CMND', label: 'CMTND' },
  { value: 'CCCD', label: 'CCCD' },
  { value: 'HC', label: 'Hộ chiếu' },
];
export const wso2Url =
  environment.BASE_WSO2_URL +
  authWSO2.authorize +
  environment.CLIENT_ID +
  authWSO2.redirect_uri +
  environment.BASE_CALLBACK_URL +
  authWSO2.scope;
export const EMAIL_VALIDATION = '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,3}$';
export const LIST_SEX = [
  { value: 1, label: 'Nam' },
  { value: 2, label: 'Nữ' },
  { value: 3, label: 'Không xác định' },
];
export const LIST_METHOD = [
  { value: 1, label: 'Hard token' },
  { value: 2, label: 'Soft token' },
];

export const LIST_AGENT = [
  { value: 1, label: 'Savis' },
  { value: 2, label: 'Viettel' },
];

export const LIST_CA_SUPPLIER = [{ value: 1, label: 'TrustCA 256(SHA256)' }];

export const LIST_FONT = [
  { label: 'Arial', fontFamily: 'Arial', fontStyle: 'normal', fontWeight: 'normal' },
  // { value: 2, label: 'Arial Bold', fontFamily: 'Arial', fontStyle: 'normal', fontWeight: 'bold' },
  // { value: 3, label: 'Arial Italic', fontFamily: 'Arial', fontStyle: 'italic', fontWeight: 'normal' },
  // { value: 4, label: 'Arial Bold Italic', fontFamily: 'Arial', fontStyle: 'italic', fontWeight: 'bold' },

  { label: 'Calibri', fontFamily: 'Calibri', fontStyle: 'normal', fontWeight: 'normal' },
  { label: 'Calibri Bold', fontFamily: 'Calibri', fontStyle: 'normal', fontWeight: 'bold' },
  { label: 'Calibri Italic', fontFamily: 'Calibri', fontStyle: 'italic', fontWeight: 'normal' },
  { label: 'Calibri Bold Italic', fontFamily: 'Calibri', fontStyle: 'italic', fontWeight: 'bold' },

  { label: 'Times New Roman', fontFamily: 'Times New Roman', fontStyle: 'normal', fontWeight: 'normal' },
  { label: 'Times New Roman Bold', fontFamily: 'Times New Roman', fontStyle: 'normal', fontWeight: 'bold' },
  { label: 'Times New Roman Italic', fontFamily: 'Times New Roman', fontStyle: 'italic', fontWeight: 'normal' },
  { label: 'Times New Roman Bold Italic', fontFamily: 'Times New Roman', fontStyle: 'italic', fontWeight: 'bold' },

  { value: 13, label: 'Verdana', fontFamily: 'Verdana', fontStyle: 'normal', fontWeight: 'normal' },

  { value: 14, label: 'Tahoma', fontFamily: 'Tahoma', fontStyle: 'normal', fontWeight: 'normal' },
  { value: 15, label: 'Tahoma Bold', fontFamily: 'Tahoma', fontStyle: 'normal', fontWeight: 'bold' },
];

export const LIST_TIME_ZONE = [
  { value: 1, label: 'Full date', eg: 'Thứ 4, 12 tháng 05 2021 11:05:45 UTC+7' },
  { value: 2, label: 'Short date', eg: '12/05/2021' },
  { value: 3, label: 'ISO 8601 Date', eg: '12/05/2021 11:05:45 UTC+7' },
];

export const QUERY_FILTER_DEFAULT: QueryFilerModel = {
  pageNumber: 1,
  pageSize: 20,
  textSearch: undefined,
};

export const PAGE_SIZE_OPTION_DEFAULT = [5, 10, 20, 50, 100];

export const EXCEL_STYLES_DEFAULT = [
  {
    id: 'greenBackground',
    interior: {
      color: '#b5e6b5',
      pattern: 'Solid',
    },
  },
  {
    id: 'redFont',
    font: {
      fontName: 'Calibri Light',
      underline: 'Single',
      italic: true,
      color: '#ff0000',
    },
  },
  {
    id: 'darkGreyBackground',
    interior: {
      color: '#888888',
      pattern: 'Solid',
    },
    font: {
      fontName: 'Calibri Light',
      color: '#ffffff',
    },
  },
  {
    id: 'boldBorders',
    borders: {
      borderBottom: {
        color: '#000000',
        lineStyle: 'Continuous',
        weight: 3,
      },
      borderLeft: {
        color: '#000000',
        lineStyle: 'Continuous',
        weight: 3,
      },
      borderRight: {
        color: '#000000',
        lineStyle: 'Continuous',
        weight: 3,
      },
      borderTop: {
        color: '#000000',
        lineStyle: 'Continuous',
        weight: 3,
      },
    },
  },
  {
    id: 'header',
    interior: {
      color: '#CCCCCC',
      pattern: 'Solid',
    },
    alignment: {
      vertical: 'Center',
      horizontal: 'Center',
    },
    font: {
      bold: true,
      fontName: 'Calibri',
    },
    borders: {
      borderBottom: {
        color: '#5687f5',
        lineStyle: 'Continuous',
        weight: 1,
      },
      borderLeft: {
        color: '#5687f5',
        lineStyle: 'Continuous',
        weight: 1,
      },
      borderRight: {
        color: '#5687f5',
        lineStyle: 'Continuous',
        weight: 1,
      },
      borderTop: {
        color: '#5687f5',
        lineStyle: 'Continuous',
        weight: 1,
      },
    },
  },
  {
    id: 'dateFormat',
    dataType: 'dateTime',
    numberFormat: { format: 'mm/dd/yyyy;@' },
  },
  {
    id: 'twoDecimalPlaces',
    numberFormat: { format: '#,##0.00' },
  },
  {
    id: 'textFormat',
    dataType: 'string',
  },
  {
    id: 'bigHeader',
    font: { size: 25 },
  },
];
export const OVERLAY_LOADING_TEMPLATE = '<span class="ag-overlay-loading-center">Đang tải dữ liệu, vui lòng chờ!</span>';
export const OVERLAY_NOROW_TEMPLATE =
  '<span style="padding: 10px 30px; border: 1px solid #f5dfdf; background: lightgoldenrodyellow;">Không có dữ liệu!</span>';
export class ConstantsCommon {
  public static HTTP_STATUS_200 = 1;
  public static HTTP_STATUS_400 = 400;
  public static HTTP_STATUS_500 = 500;
  public static HTTP_STATUS_401 = 401;
  public static HTTP_STATUS_403 = 403;
}

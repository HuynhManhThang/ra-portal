export const authenticationRouter = {
  getToken: `api/v1/authentication/jwt/login`,
};

export const navigationRouter = {
  getNavigationOwner: `api/v1/bsd/navigations/owner`,
};

export const nodeUploadRouter = {
  uploadFileBlobPhysical: `api/v1/core/nodes/upload/physical/blob`,
  uploadFileBinary: `api/v1/core/minio/upload-object`,
  downloadFile: `api/v1/core/minio/download-object`,
};

export const unitRouter = {
  create: `api/v1/res/unit`,
  createMany: `api/v1/res/unit/create-many`,
  update: `api/v1/res/unit`,
  delete: `api/v1/res/unit`,
  getById: `api/v1/res/unit?id=`,
  getFilter: `api/v1/res/unit/filter`,
  getAll: `api/v1/res/unit/all`,
  getListCombobox: `api/v1/res/unit/for-combobox`,
};

export const servicePackageRouter = {
  create: `api/v1/service-pack`,
  generateCode: `api/v1/service-pack/generate-code`,
  createMany: `api/v1/service-pack/create-many`,
  update: `api/v1/service-pack`,
  delete: `api/v1/service-pack`,
  getById: `api/v1/service-pack?id=`,
  getFilter: `api/v1/service-pack/filter`,
  getAll: `api/v1/service-pack/all`,
  getListCombobox: `api/v1/service-pack/for-combobox`,
};

export const countryRouter = {
  create: `api/v1/country`,
  createMany: `api/v1/country/create-many`,
  update: `api/v1/country`,
  delete: `api/v1/country`,
  getById: `api/v1/country?id=`,
  getFilter: `api/v1/country/filter`,
  getAll: `api/v1/country/all`,
  getListCombobox: `api/v1/country/for-combobox`,
};

export const provinceRouter = {
  create: `api/v1/province`,
  createMany: `api/v1/province/create-many`,
  update: `api/v1/province`,
  delete: `api/v1/province`,
  getById: `api/v1/province?id=`,
  getFilter: `api/v1/province/filter`,
  getAll: `api/v1/province/all`,
  getListCombobox: `api/v1/province/for-combobox`,
};
export const usersRouter = {
  create: `api/v1/user`,
  createMany: `api/v1/user/create-many`,
  update: `api/v1/user`,
  delete: `api/v1/user`,
  lock: `api/v1/user/lock`,
  getById: `api/v1/user?id=`,
  getFilter: `api/v1/user/filter`,
  getAll: `api/v1/user/all`,
  getListCombobox: `api/v1/user/for-combobox`,
  getUserOrOrgInfo: `api/v1/user/get-user-org-info`,
};

export const authWSO2 = {
  authorize: 'oauth2/authorize?response_type=code&client_id=',
  redirect_uri: '&redirect_uri=',
  scope: '&scope=openid',
  authenticationInfoByCode: '/authenticationinfo?_allow_anonymous=true',
  logout: 'logout/',
  logoutWso2: 'oidc/logout?id_token_hint=',
  refreshUrl: '/refreshtoken?_allow_anonymous=true',
  post_logout_redirect_uri: '&post_logout_redirect_uri=',
};
export const userRouter = {
  getListRightOfUser: `api/v1/idm/users`,
  getListRoleOfUser: `api/v1/idm/users`,
};
export const AgencyRouter = {
  create: `api/v1/agency`,
  getById: `api/v1/agency/`,
  getFilter: `api/v1/agency/filter`,
  getALL: `api/v1/agency/filter`,
};
export const RegisterAccountRouter = {
  create: `api/v1/user`,
  getById: `api/v1/user/info`,
};
export const CertificateRouter = {
  create: `api/v1/certificate`,
  getById: `api/v1/certificate/`,
  getFilter: `api/v1/certificate/filter`,
  getALL: `api/v1/certificate/filter`,
};

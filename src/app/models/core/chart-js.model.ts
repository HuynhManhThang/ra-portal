import { Component } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

export class ChartJSModel {
  title: string | any;
  data: ChartDataSets[] = [];
  labels: Label[] | any[] = [];
  options: any;
  colors: Color[] | any[] = [];
  type: string | any;
  dataOrigin: any;
  ready = false;
  [key: string]: any;
}

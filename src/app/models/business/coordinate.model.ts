export class CoordinateModel {
  llx!: number;
  lly!: number;
  width!: number;
  height!: number;
  scaleX!: number;
  scaleY!: number;
}

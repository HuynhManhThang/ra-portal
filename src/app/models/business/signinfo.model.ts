export interface SignInfoModel {
  status?: boolean;
  isShowLabel?: boolean;
  label?: string;
  value?: string;
  llx?: number;
  lly?: number;
  width?: number;
  height?: number;
  font?: FontModel;
  fontSize?: number;
  fontColor?: string;
  timeFormat?: string;
  scale?: number;
  scaleX?: number;
  scaleY?: number;
}

export class FontModel {
  label?: string | undefined | null;
  fontFamily?: string;
  fontStyle?: string;
  fontWeight?: string;
}

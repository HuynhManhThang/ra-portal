import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NgZorroAntdModule } from '../../ng-zorror-ant/ng-zorro-antd.module';
import { BtnCellRenderComponent } from './btn-cell-render/btn-cell-render.component';
import { DateCellRenderComponent } from './date-cell-render/date-cell-render.component';
import { IsRequiredCellRenderComponent } from './is-required-cell-render/is-required-cell-render.component';
import { NumberButtonCellRenderComponent } from './number-button-cell-render/number-button-cell-render.component';
import { StatusCellRenderComponent } from './status-cell-render/status-cell-render.component';
import { StatusDeleteCellRenderComponent } from './status-delete-cell-render/status-delete-cell-render.component';
import { StatusImportCellRenderComponent } from './status-import-cell-render/status-import-cell-render.component';
import { StatusNameCellRenderComponent } from './status-name-cell-render/status-name-cell-render.component';
import { StatusSignResponseCellRenderComponent } from './status-sign-response-cell-render/status-sign-response-cell-render.component';

const COMPONENTS = [
  DateCellRenderComponent,
  StatusCellRenderComponent,
  StatusCertCellRenderComponent,
  StatusNameCellRenderComponent,
  ChangeStateCellRenderComponent,
  StatusDeleteCellRenderComponent,
  StatusImportCellRenderComponent,
  StatusSignResponseCellRenderComponent,
  IsRequiredCellRenderComponent,
  BtnCellRenderComponent,
  NumberButtonCellRenderComponent,
];

import { FormsModule } from '@angular/forms';
import { AgGridModule } from 'ag-grid-angular';
import { ChangeStateCellRenderComponent } from './change-state-cell-render/change-state-cell-render.component';
import { StatusCertCellRenderComponent } from './status-cert-cell-render/status-cert-cell-render.component';

@NgModule({
  imports: [
    CommonModule,
    NzSwitchModule,
    FormsModule,
    NgZorroAntdModule,
    AgGridModule.withComponents([
      DateCellRenderComponent,
      StatusCellRenderComponent,
      StatusNameCellRenderComponent,
      StatusDeleteCellRenderComponent,
      StatusCertCellRenderComponent,
      StatusImportCellRenderComponent,
      ChangeStateCellRenderComponent,
      StatusSignResponseCellRenderComponent,
      IsRequiredCellRenderComponent,
      BtnCellRenderComponent,
      NumberButtonCellRenderComponent,
    ]),
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class CellRenderModule {}

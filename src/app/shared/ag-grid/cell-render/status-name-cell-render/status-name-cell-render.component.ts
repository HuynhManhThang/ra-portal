import { Component, OnDestroy, OnInit } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { IAfterGuiAttachedParams } from 'ag-grid-community';

@Component({
  selector: 'app-status-name-cell-render',
  templateUrl: './status-name-cell-render.component.html',
  styleUrls: [],
})
export class StatusNameCellRenderComponent implements ICellRendererAngularComp, OnInit, OnDestroy {
  constructor() {}
  status: any;
  params: any;

  refresh(params: any): boolean {
    throw new Error('Method not implemented.');
  }
  afterGuiAttached?(params?: IAfterGuiAttachedParams): void {
    throw new Error('Method not implemented.');
  }

  ngOnInit(): void {}

  agInit(params: any): void {
    switch (params.value) {
      case 'Đang hoạt động':
        this.status = true;
        break;
      case 'Ngừng hoạt động':
        this.status = false;
        break;
      default:
        break;
    }
    this.params = params;
  }
  btnStatusClickedHandler(event: any): any {
    // console.log(this.params.data);
    this.params.changeStatus(event, this.params.data.id);
  }
  ngOnDestroy(): any {
    // no need to remove the button click handler
    // https://stackoverflow.com/questions/49083993/does-angular-automatically-remove-template-event-listeners
  }
}

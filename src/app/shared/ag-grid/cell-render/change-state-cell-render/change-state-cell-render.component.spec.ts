import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeStateCellRenderComponent } from './change-state-cell-render.component';

describe('ChangeStateCellRenderComponent', () => {
  let component: ChangeStateCellRenderComponent;
  let fixture: ComponentFixture<ChangeStateCellRenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChangeStateCellRenderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeStateCellRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

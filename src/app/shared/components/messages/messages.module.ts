import { NgModule } from '@angular/core';

import { MessagesComponent } from './messages.component';

const COMPONENTS = [MessagesComponent];

@NgModule({
  imports: [],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class MessagesModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { ChangeStateModalComponent } from './change-state-modal/change-state-modal.component';

import { AgGridModule } from 'ag-grid-angular';
import { NgZorroAntdModule } from '../../ng-zorror-ant/ng-zorro-antd.module';

const COMPONENTS = [DeleteModalComponent, ChangeStateModalComponent];

@NgModule({
  imports: [CommonModule, NgZorroAntdModule, AgGridModule.withComponents([])],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class ModalModule {}

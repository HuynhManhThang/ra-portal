import { NgModule } from '@angular/core';

import { LoaderComponent } from './loader.component';

const COMPONENTS = [LoaderComponent];

@NgModule({
  imports: [],
  declarations: COMPONENTS,
  exports: COMPONENTS,
})
export class LoaderModule {}

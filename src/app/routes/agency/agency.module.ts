import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';

import { AgGridModule } from 'ag-grid-angular';
import { NgZorroAntdModule } from 'src/app/shared/ng-zorror-ant/ng-zorro-antd.module';
import { AgencyRoutingModule } from './agency-routing.module';
import { AgencyListComponent } from './agency-list/agency-list.component';
import { AgencyItemComponent } from './agency-item/agency-item.component';
import { DatePipe } from '@angular/common';

// dashboard pages

const COMPONENTS: Type<void>[] = [AgencyListComponent, AgencyItemComponent];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), AgencyRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
  providers: [DatePipe],
})
export class AgencyModule {}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgencyItemComponent } from './agency-item/agency-item.component';
import { AgencyListComponent } from './agency-list/agency-list.component';
// layout

// dashboard pages

const routes: Routes = [
  { path: '', component: AgencyListComponent },
  { path: 'item', component: AgencyItemComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class AgencyRoutingModule { }

import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';

import { AgGridModule } from 'ag-grid-angular';
import { RegisterRoutingModule } from './register-routing.module';
import { NgZorroAntdModule } from 'src/app/shared/ng-zorror-ant/ng-zorro-antd.module';
import { RegisterAccountComponent } from './register-account.component';
import { RegisterPersonalComponent } from './register-personal/register-personal.component';
import { RegisterOrganizationComponent } from './register-organization/register-organization.component';
import { TakeAPhotoComponent } from './take-aphoto/take-aphoto.component';
import { WebcamModule } from 'ngx-webcam';

// dashbconst COMPON
const COMPONENTS: Type<void>[] = [RegisterAccountComponent, RegisterPersonalComponent, RegisterOrganizationComponent, TakeAPhotoComponent];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), RegisterRoutingModule, WebcamModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class RegisterModule {}

import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ACLService } from '@delon/acl';
import { ButtonModel } from '@model';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable, Subject, Subscription } from 'rxjs';

import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { cleanForm, ConstantsCommon, ROLE_SYS_ADMIN } from '@util';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UnitApiService } from '@service';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';

@Component({
  selector: 'app-take-aphoto',
  templateUrl: './take-aphoto.component.html',
  styleUrls: ['./take-aphoto.component.less'],
})
export class TakeAPhotoComponent implements OnInit {
  @Input() type = 'add';
  @Input() item: any;
  @Input() isVisible = false;
  @Input() option: any;
  @Output() eventEmmit = new EventEmitter<any>();

  moduleName = 'đại lý';

  isInfo = false;
  isEdit = false;
  isAdd = false;
  tittle = '';

  isLoading = false;
  isReloadGrid = false;

  listOrganization: any[] = [];
  listPosition: any[] = [];

  isChangePass = false;

  btnSave: ButtonModel;
  btnSaveAndCreate: ButtonModel;
  btnCancel: ButtonModel;
  btnEdit: ButtonModel;

  constructor(
    private fb: FormBuilder,
    private messageService: NzMessageService,
    private userApiService: UnitApiService,
    private notification: NzNotificationService,
    private aclService: ACLService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
  ) {
    this.btnSave = {
      title: 'Lưu',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.save();
      },
    };
    this.btnSaveAndCreate = {
      title: 'Lưu & Thêm mới',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.save();
      },
    };
    this.btnCancel = {
      title: 'Đóng',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.handleCancel();
      },
    };
    this.btnEdit = {
      title: 'Cập nhật',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        // this.updateFormType('edit');
      },
    };

  }
  public seconds: number = 0;
  private trigger: Subject<void> = new Subject<void>();
  private nextWebcam: Subject<boolean | string> = new Subject<boolean | string>();
  // latest snapshot
  public webcamImage?: WebcamImage;


  ngOnInit(): void { }
  handleCancel(): void {
    this.isVisible = false;
    if (this.isReloadGrid) {
      this.eventEmmit.emit({ type: 'success' });
    } else {
      this.eventEmmit.emit({ type: 'close' });
    }
  }

  initRightOfUser(): void {
    console.log('ok');

    // this.btnSave.grandAccess = this.aclService.canAbility('UNIT-CREATE');
    // this.btnEdit.grandAccess = this.aclService.canAbility('UNIT-EDIT');
    // this.btnSaveAndCreate.grandAccess = this.aclService.canAbility('UNIT-CREATE');
  }

  public initData(data?: any, type: any = null, option: any = {}): void {
    this.isLoading = false;
    this.isChangePass = false;
    this.isReloadGrid = false;
    this.item = data;
    this.type = type;
    this.tittle = 'Chụp ảnh';
  }

  closeModalReloadData(): void {
    this.isVisible = false;
    this.eventEmmit.emit({ type: 'success' });
  }


  save() {
    this.triggerSnapshot()
    this.isVisible = false;
    this.eventEmmit.emit({ type: 'success', data: this.webcamImage?.imageAsDataUrl });
  }

  public triggerSnapshot(): void {
    this.trigger.next();
  }
  public showNextWebcam(directionOrDeviceId: boolean | string): void {
    this.nextWebcam.next(directionOrDeviceId);
  }
  public handleImage(webcamImage: WebcamImage): void {
    this.webcamImage = webcamImage;
  }

  public get triggerObservable(): Observable<void> {
    return this.trigger.asObservable();
  }
}

import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { environment } from '@env/environment';
import { CountryApiService } from '@service';
import { nodeUploadRouter } from '@util';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';
import { StepService } from 'src/app/services/api/steps.service';
import { TakeAPhotoComponent } from '../take-aphoto/take-aphoto.component';
import { RegisterAccountService } from 'src/app/services/api/register-acount-api.service';

@Component({
  selector: 'app-register-personal',
  templateUrl: './register-personal.component.html',
  styles: [
    `
      nz-sider {
        background: #fff;
        margin: 0 10px 0 0;
      }

      nz-content {
        min-height: 90vh;
        background: #fff;
      }

      nz-layout {
        margin: 0 10px;
        margin-bottom: 48px;
      }
      ul {
        padding: 0 16px;
      }
    `,
  ],
})
export class RegisterPersonalComponent implements OnInit {
  @ViewChild(TakeAPhotoComponent, { static: false }) itemModal!: { initData: (arg0?: {}, arg1?: string, option?: any) => void };
  modal: any = {
    type: '',
    item: {},
    isShow: false,
    option: {},
  };
  loading: boolean = false;
  dataTT = ['NHẬP THÔNG TIN ĐĂNG KÝ', 'XÁC THỰC THÔNG TIN', 'THÔNG TIN TRUY CẬP VÀ DỊCH VỤ'];
  title = this.dataTT[0];
  date = null;
  checkOptionsOne = [
    { label: 'Dịch vụ cấp chứng thư số', value: 1 },
    { label: 'Dịch vụ ký hợp đồng điện tử', value: 2 },
    { label: 'Dịch vụ xác thực qua OTP', value: 3 },
    { label: 'Dịch vụ vay tiêu dùng', value: 4 },
  ];
  index = 0;
  disable = false;

  // nationIDFileList: NzUploadFile[] = [];
  header = {};
  uploadUrl = environment.API_URL + nodeUploadRouter.uploadFileBinary;

  // variable form
  backIdentityImage = '../../../assets/tmp/img/id-card.svg';
  frontIdentityImage = '../../../assets/tmp/img/id-card.svg';
  avt = '../../../assets/user.svg';
  registerPesonFrom: FormGroup;
  uploadType?: Boolean;
  takeaphoto: any;
  imageDefault: any;
  sex: any = '1';
  extensions: any = [];
  commitment: boolean = false;
  dataListCity: any;
  passwordVisible = false;
  constructor(
    private msg: NzMessageService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private stepSevice: StepService,
    private countryService: CountryApiService,
    private fb: FormBuilder,
    private registerAccountService: RegisterAccountService,
  ) {
    const token = tokenService.get()?.token;
    if (token) {
      this.header = {
        Authorization: 'Bearer ' + token,
      };
    }
    this.registerPesonFrom = this.fb.group({
      avatar: [null, [Validators.required]],
      backIdentityImage: [null, [Validators.required]],
      birthday: [null, [Validators.required]],
      countryId: [null, [Validators.required]],
      countryName: [null, [Validators.required]],
      currentAddress: [null, [Validators.required]],
      cutFromIdentityImage: [null, [Validators.required]],
      email: [null, [Validators.required]],
      frontIdentityImage: [null, [Validators.required]],
      identityNumber: [null, [Validators.required]],
      identityType: 0,
      issuerBy: [null, [Validators.required]],
      issuerDate: [null, [Validators.required]],
      name: [null, [Validators.required]],
      permanentAddress: [null, [Validators.required]],
      phoneNumber: [null, [Validators.required]],
      positionName: [null, [Validators.required]],
      province: [null, [Validators.required]],
      provinceId: [null, [Validators.required]],
      provinceName: [null, [Validators.required]],
      username: [null, [Validators.required]],
      password: [null, [Validators.required]],
      confimPassword: [null, [Validators.required, this.confirmationValidator]],
      sex: 0,
    });
  }

  ngOnInit() {
    this.stepSevice.getStep.subscribe((res) => {
      this.index = +res;
      this.indexTilte(this.index);
    });
    this.getCountry();
  }
  updateConfirmValidator(): void {
    /** wait for refresh value */
    Promise.resolve().then(() => this.registerPesonFrom.controls.confimPassword.updateValueAndValidity());
  }

  confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { required: true };
    } else if (control.value !== this.registerPesonFrom.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  };

  getCountry() {
    this.countryService.getAPICity().subscribe((res: any) => (this.dataListCity = res));
  }
  onIndexChange(index: number): void {
    this.index = index;
  }
  pre(): void {
    this.index -= 1;
    this.stepSevice.setStep(this.index);
    this.indexTilte(this.index);
  }

  next(): void {
    this.index += 1;
    this.stepSevice.setStep(this.index);
    this.indexTilte(this.index);
  }

  done(): void {
    for (const i in this.registerPesonFrom.controls) {
      if (this.registerPesonFrom.controls.hasOwnProperty(i)) {
        this.registerPesonFrom.controls[i].markAsDirty();
        this.registerPesonFrom.controls[i].updateValueAndValidity();
      }
    }
    this.extensions = [];
    const formValue = this.registerPesonFrom.value;
    this.checkOptionsOne.map((item: any) => {
      if (item.checked) this.extensions = [item.value, ...this.extensions];
    });
    formValue.backIdentityImage = this.backIdentityImage;
    formValue.frontIdentityImage = this.frontIdentityImage;
    formValue.avatar = this.avt;
    formValue.provinceId = formValue.province?.ID;
    formValue.countryName = 'Việt Nam';
    var data = {
      commitment: this.commitment,
      extensions: this.extensions.toString(),
      password: formValue.password,
      userInfo: { ...formValue },
      userType: 1,
      username: formValue.username,
    };
    this.loading = true;
    this.registerAccountService.create(data).subscribe(
      (res: any) => {
        if (res.code == 1) {
          this.msg.success('Chúc mừng đã đăng ký tài khoản cá nhân thành công');
          this.stepSevice.setStep(0);
          this.loading = false;
          this.backIdentityImage = this.frontIdentityImage = '../../../assets/tmp/img/id-card.svg';
          this.avt = '../../../assets/user.svg';
          this.registerPesonFrom.reset();
        } else {
          this.msg.success('Đăng ký tài khoản thất bại. Vui lòng thử lại!');
          this.loading = false;
        }
      },
      (error: any) => {
        this.msg.success('Đăng ký tài khoản thất bại. Vui lòng thử lại!');

        console.log(
          `🚀 ~ file: register-personal.component.ts ~ line 179 ~ RegisterPersonalComponent ~ this.registerAccountService.create ~ error`,
          error,
        );
        this.loading = false;
      },
    );
  }

  indexTilte(i: any) {
    if (i == 0) {
      this.title = this.dataTT[0];
    } else if (i == 1) {
      this.title = this.dataTT[1];
    } else if (i == 2) {
      this.title = this.dataTT[2];
    }
  }

  private getBase64(img: File, callback: (img: string) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result!.toString()));
    reader.readAsDataURL(img);
  }

  handleChangeFile(info: NzUploadChangeParam, id: any): void {
    let fileList = [...info.fileList];
    fileList = fileList.slice(-1);
    fileList = fileList.map((file) => {
      if (file.response) {
        file.url = file.response.url;
      }
      return file;
    });
    this.getBase64(info.file!.originFileObj!, (img: string) => {
      this.uploadType = true;
      if (id === 'backIdentityImage') {
        this.backIdentityImage = img;
      }
      if (id === 'frontIdentityImage') {
        this.frontIdentityImage = img;
      }
      if (id === 'avatar') {
        this.avt = img;
      }
    });
  }
  log(value: object[]): void {
    console.log(value);
  }
  OnTakeaPhoto(e: Event, id: any) {
    e.stopPropagation();
    this.uploadType = false;
    this.takeaphoto = id;
    this.modal = {
      type: 'add',
      item: {},
      isShow: true,
      option: {},
    };
    this.itemModal.initData('add');
  }
  onModalEventEmmit(event: any): void {
    this.modal.isShow = false;
    if (event.type === 'success') {
      if (this.takeaphoto === 'backIdentityImage') {
        this.backIdentityImage = event.data;
      }
      if (this.takeaphoto === 'frontIdentityImage') {
        this.frontIdentityImage = event.data;
      }
      if (this.takeaphoto === 'avatar') {
        this.avt = event.data;
      }
    }
  }
}

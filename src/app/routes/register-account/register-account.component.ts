import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { StepService } from 'src/app/services/api/steps.service';

@Component({
  selector: 'app-register-account',
  templateUrl: './register-account.component.html',
  styles: [`
  nz-sider {
  background: #fff;
  margin:0 18px 0 0;
  border-radius:10px
}

nz-content {
  min-height:400px;
  background:#fff;
  border-radius:10px
}

nz-layout {
  margin:0 18px;
  margin-bottom: 48px;
}
ul{
padding:0 16px
}
`]
})
export class RegisterAccountComponent implements OnInit {

  register: boolean = true;
  index = 0;
  dataStep: any;
  constructor(private msg: NzMessageService,
    private router: Router,
    private aRouter: ActivatedRoute,
    private stepSevice: StepService,
  ) {
    this.register = this.aRouter.snapshot.routeConfig?.path == 'organization' ? false : true
  }
  ngOnInit() {
    this.stepSevice.getStep.subscribe(res => {
      this.index = +res
    })
  }

  onIndexChange(index: number): void {
    this.index = index;
    this.stepSevice.setStep(index)
  }

}

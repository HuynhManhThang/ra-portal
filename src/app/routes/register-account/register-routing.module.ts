import { LayoutProComponent } from '@brand';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
// layout

// dashboard pages
import { JWTGuard } from '@delon/auth';
import { RegisterAccountComponent } from './register-account.component';
import { RegisterPersonalComponent } from './register-personal/register-personal.component';
import { RegisterOrganizationComponent } from './register-organization/register-organization.component';

const routes: Routes = [
  { path: '', redirectTo: 'personal', pathMatch: 'full' },
  { path: 'personal', component: RegisterAccountComponent },
  { path: 'organization', component: RegisterAccountComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class RegisterRoutingModule { }

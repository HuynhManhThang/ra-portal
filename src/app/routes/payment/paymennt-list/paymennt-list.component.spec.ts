/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PaymenntListComponent } from './paymennt-list.component';

describe('PaymenntListComponent', () => {
  let component: PaymenntListComponent;
  let fixture: ComponentFixture<PaymenntListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymenntListComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymenntListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';

import { AgGridModule } from 'ag-grid-angular';
import { NgZorroAntdModule } from 'src/app/shared/ng-zorror-ant/ng-zorro-antd.module';
// import { CertificateRoutingModule } from './certificate-routing.module';
// import { CertifycateListComponent } from './certifycate-list/certifycate-list.component';
// import { CertifycateItemComponent } from './certifycate-item/certifycate-item.component';
import { PaymenntListComponent } from './paymennt-list/paymennt-list.component';
//import { PaymentItemComponent } from './payment-item/payment-item.component';
import { PaymentRoutingModule } from './payment-routing.module';

// dashboard pages

const COMPONENTS: Type<void>[] = [];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), PaymentRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT, PaymenntListComponent],
  entryComponents: COMPONENTS_NOROUNT,
})
export class PaymentModule {}

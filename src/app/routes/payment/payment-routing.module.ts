// import { NgModule } from '@angular/core';
// import { RouterModule, Routes } from '@angular/router';

import { LayoutProComponent } from '@brand';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { PaymenntListComponent } from './paymennt-list/paymennt-list.component';
import { PaymentItemComponent } from './payment-item/payment-item.component';
// layout

// dashboard pages

const routes: Routes = [
  { path: '', component: PaymenntListComponent },
  { path: 'item', component: PaymentItemComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentRoutingModule {}

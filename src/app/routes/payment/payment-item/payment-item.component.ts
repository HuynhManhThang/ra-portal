// import { Component, OnInit } from '@angular/core';

import { ClientSideRowModelModule } from '@ag-grid-community/client-side-row-model';
import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ACLService } from '@delon/acl';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { ArrayService } from '@delon/util';
import { environment } from '@env/environment';
import { ButtonModel, GridModel, QueryFilerModel } from '@model';
import { CountryApiService, ProvinceApiService, ServicePackageService, UserService } from '@service';
import { BtnCellRenderComponent, DeleteModalComponent, StatusNameCellRenderComponent } from '@shared';
import { cleanForm, nodeUploadRouter } from '@util';
import {
  EXCEL_STYLES_DEFAULT,
  LIST_AGENT,
  LIST_CA_SUPPLIER,
  LIST_CERT_TYPE,
  LIST_METHOD,
  LIST_STATUS,
  OVERLAY_LOADING_TEMPLATE,
  OVERLAY_NOROW_TEMPLATE,
  PAGE_SIZE_OPTION_DEFAULT,
  QUERY_FILTER_DEFAULT,
  ROLE_SYS_ADMIN,
} from '@util';
import { constants } from 'crypto';
import * as moment from 'moment';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzUploadChangeParam } from 'ng-zorro-antd/upload';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import PerfectScrollbar from 'perfect-scrollbar';
import { Subscription } from 'rxjs';
import { PaymenntListComponent } from '../paymennt-list/paymennt-list.component';

@Component({
  selector: 'app-payment-item',
  templateUrl: './payment-item.component.html',
  styleUrls: ['./payment-item.component.less'],
})
export class PaymentItemComponent implements OnInit {
  typeCertifycate: any = '0';
  sub: any;
  id: string | null = '0';
  type: string | null = '0';

  isShowCertItemForm = true;
  isRenderComplete = false;

  filter: QueryFilerModel = { ...QUERY_FILTER_DEFAULT };
  pageSizeOptions: any[] = [];
  paginationMessage = '';

  columnDefs: any[] = [];
  grid: GridModel = {
    dataCount: 0,
    rowData: [],
    totalData: 0,
  };
  private gridApi: any;
  private gridColumnApi: any;
  modules = [ClientSideRowModelModule];
  defaultColDef: any;
  rowSelection = 'multiple';
  overlayLoadingTemplate = OVERLAY_LOADING_TEMPLATE;
  overlayNoRowsTemplate = OVERLAY_NOROW_TEMPLATE;
  quickText = '';
  excelStyles: any;
  frameworkComponents: any;

  isLoading = false;

  btnAdd: ButtonModel;
  btnResetSearch: ButtonModel;
  btnSearch: ButtonModel;
  btnReload: ButtonModel;
  btnSave: ButtonModel;
  btnSaveAndCreate: ButtonModel;
  btnCancel: ButtonModel;

  isLoadingDelete = false;
  isShowDelete = false;
  isShowImport = false;

  form: FormGroup;

  uploadUrl = environment.API_URL + nodeUploadRouter.uploadFileBinary;

  nationIDFileList: NzUploadFile[] = [];
  businessLicenseFileList: NzUploadFile[] = [];
  registrationFileList: NzUploadFile[] = [];
  confirmationFileList: NzUploadFile[] = [];

  tittle = 'Quản lý chứng thư số';

  identityType = '';
  user_id: null | undefined = null;
  organization_id: null | undefined = null;
  listCertType = LIST_CERT_TYPE;
  listOfAgent = LIST_AGENT;
  listOfCASupplier = LIST_CA_SUPPLIER;
  listOfMethod = LIST_METHOD;
  listOfServicePack: any[] = [];
  listOfCountries: any[] = [];
  listOfProvinces: any[] = [];
  header = {};
  modal: any = {
    type: '',
    item: {},
    isShow: false,
    option: {},
  };

  @ViewChild(PaymenntListComponent, { static: false }) viewCertModal!: { initData: (arg0: {}, arg1: string) => void };

  constructor(
    private fb: FormBuilder,
    private aclService: ACLService,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private arrayService: ArrayService,
    private notification: NzNotificationService,
    private messageService: NzMessageService,
    private elementRef: ElementRef,
    private certService: UserService,
    private userService: UserService,
    private servicePackService: ServicePackageService,
    private activatedRoute: ActivatedRoute,
    private countryApiService: CountryApiService,
    private provinceApiService: ProvinceApiService,
    private route: Router,
  ) {
    const token = tokenService.get()?.token;
    if (token) {
      this.header = {
        Authorization: 'Bearer ' + token,
      };
    }
    //#region get root param id && userId in token
    this.sub = this.activatedRoute.paramMap.subscribe((params) => {
      // console.log(params);
      this.id = params.get('id');
      this.type = params.get('type');
    });

    this.columnDefs = [];
    if (this.type === '3') {
      this.columnDefs.push(
        { field: 'taxCode', headerName: 'Mã số thuế', minWidth: 180, flex: 1 },
        { field: 'commonName', headerName: 'Tên cá nhân', sortable: true, filter: true, minWidth: 180, flex: 1 },
      );
    } else {
      this.columnDefs.push(
        { field: 'commonName', headerName: 'Tên cá nhân', minWidth: 180, flex: 1 },
        { field: 'identityNumber', headerName: 'Số CMND', sortable: true, filter: true, minWidth: 180, flex: 1 },
      );
    }

    //#region ag-grid
    this.columnDefs.push(
      { field: 'agency', headerName: 'Đại lý', sortable: true, filter: true, minWidth: 180, flex: 1 },
      { field: 'createdDate', headerName: 'Ngày tạo', sortable: true, filter: true, minWidth: 180, flex: 1 },
      {
        headerName: 'Thao tác',
        minWidth: 150,
        cellRenderer: 'btnCellRender',
        cellRendererParams: {
          downloadCertClicked: (item: any) => this.onDownloadCert(item),
          infoClicked: (item: any) => this.onViewCert(item),
        },
      },
    );
    this.defaultColDef = {
      // flex: 1,
      minWidth: 100,
      resizable: true,
    };
    this.frameworkComponents = {
      btnCellRender: BtnCellRenderComponent,
    };
    //#endregion ag-grid

    //#region Init button
    this.btnAdd = {
      title: 'Thêm mới',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.onAddItem();
      },
    };

    this.btnSearch = {
      title: 'Tìm kiếm',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.initGridData();
      },
    };
    this.btnResetSearch = {
      title: 'Đặt lại',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.onResetSearch(false);
      },
    };
    this.btnReload = {
      title: 'Tải lại',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.onResetSearch(true);
      },
    };
    this.btnCancel = {
      title: 'Hủy',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.onCancel();
      },
    };

    this.btnSave = {
      title: 'Lưu',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.save();
      },
    };
    this.btnSaveAndCreate = {
      title: 'Lưu & Thêm mới',
      titlei18n: '',
      visible: true,
      enable: true,
      grandAccess: true,
      click: ($event: any) => {
        this.save(true);
      },
    };

    //#endregion Init button

    this.form = this.fb.group({
      // Fieldset 1
      userName: [null], // Tài khoản
      commonName: [{ value: '', disabled: true }], // Tên đơn vị-Tên cá nhân
      identityNumber: [{ value: '', disabled: true }], // CMND
      taxCode: [{ value: '', disabled: true }], // MST
      organizationUnit: [{ value: '', disabled: true }], // Đơn vị tổ chức (OU)
      email: [{ value: '', disabled: true }], // Email
      address: [{ value: '', disabled: true }], // Địa chỉ
      phoneNumber: [{ value: '', disabled: true }], // Số điện thoại
      organizationName: [{ value: '', disabled: true }], // Tổ chức
      countryName: [{ value: '', disabled: true }], // Quốc gia
      provinceName: [{ value: '', disabled: true }], // Tỉnh / TP
      positionName: [{ value: '', disabled: true }], // Chức vụ

      // Fieldset 2
      agency: [null, [Validators.required]], // Đại lý
      tokenMethod: [null, [Validators.required]], // Phương thức
      caSupplier: [null, [Validators.required]], // Nhà cung cấp CA
      servicePack: [null, [Validators.required]], // Gói dịch vụ
      price: [{ value: '', disabled: true }, [Validators.required]], // Số tiền phí (VNĐ)

      // Fieldset 3
      // nationIDFile: [null, [Validators.required]], // Giấy CMND/CCCD
      // businessLicenseFile: [null, [Validators.required]], // Giấy phép kinh doanh
      // registrationFile: [null, [Validators.required]], // Giấy đăng ký
      // confirmationFile: [null, [Validators.required]], // Giấy xác nhận
    });

    // Gán filter
    this.filter.objectId = this.id;
    this.filter.certType = Number(this.type);
  }

  async ngOnInit(): Promise<void> {
    // this.initRightOfUser();
    // this.initListOfServicePack();
    // await this.initListCountries();
    // await this.initListProvinces();
    // this.initCommonNameInfo();
    this.isRenderComplete = true;
  }

  initRightOfUser(): void {
    // this.btnAdd.grandAccess = this.aclService.canAbility('UNIT-ADD');
    // this.btnDelete.grandAccess = this.aclService.canAbility('UNIT-DELETE');
    // this.btnExportExcel.grandAccess = this.aclService.canAbility('UNIT-EXPORT-EXCEL');
  }

  initCommonNameInfo(): Subscription | undefined {
    const promise = this.userService.getUserOrOrgInfo(this.id, this.type).subscribe(
      (res: any) => {
        if (res.code !== 200) {
          this.messageService.error(`${res.message}`);
          return;
        }
        if (res.data === null || res.data === undefined) {
          this.messageService.error(`${res.message}`);
          return;
        }
        const dataResult = res.data;
        // console.log(dataResult);
        this.fillForm(dataResult);
      },
      (err: any) => {
        if (err.error) {
          this.messageService.error(`${err.error.message}`);
        } else {
          this.messageService.error(`${err.status}`);
        }
      },
    );
    return promise;
  }

  async initListCountries(): Promise<any> {
    const res = await this.countryApiService.getListCombobox().toPromise();
    if (res.code !== 200) {
      this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
      return;
    }
    if (res.data === null || res.data === undefined) {
      this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
      return;
    }
    this.listOfCountries = res.data;
    return res.data;
  }

  async initListProvinces(): Promise<any> {
    const res = await this.provinceApiService.getListCombobox().toPromise();
    if (res.code !== 200) {
      this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
      return;
    }
    if (res.data === null || res.data === undefined) {
      this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
      return;
    }
    this.listOfProvinces = res.data;
    return res.data;
  }

  initListOfServicePack(): Subscription | undefined {
    const promise = this.servicePackService.getListCombobox().subscribe(
      (res: any) => {
        if (res.code !== 200) {
          this.messageService.error(`${res.message}`);
          return;
        }
        if (res.data === null || res.data === undefined) {
          this.messageService.error(`${res.message}`);
          return;
        }
        const dataResult = res.data;
        this.listOfServicePack = dataResult;
      },
      (err: any) => {
        if (err.error) {
          this.messageService.error(`${err.error.message}`);
        } else {
          this.messageService.error(`${err.status}`);
        }
      },
    );
    return promise;
  }

  //#region Ag-grid
  onPageSizeChange(): void {
    this.initGridData();
  }

  onPageNumberChange(): void {
    this.initGridData();
  }

  onGridReady(params: any): void {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    // this.approvePerfectScrollbar();
    this.initGridData();
  }

  rollBackHistory(): void {
    if (this.type === '3') {
      this.route.navigateByUrl('/system/organization');
    } else {
      this.route.navigateByUrl('/system/user');
    }
  }

  approvePerfectScrollbar(): void {
    const agBodyViewport: HTMLElement = this.elementRef.nativeElement.querySelector('.ag-body-viewport');
    const agBodyHorizontalViewport: HTMLElement = this.elementRef.nativeElement.querySelector('.ag-body-horizontal-scroll-viewport');
    if (agBodyViewport) {
      const ps = new PerfectScrollbar(agBodyViewport);
      ps.update();
    }
    if (agBodyHorizontalViewport) {
      const ps = new PerfectScrollbar(agBodyHorizontalViewport);
      ps.update();
    }
  }

  onSelectionChanged($event: any): void {
    const selectedRows = this.gridApi.getSelectedRows();
    if (selectedRows.length > 0) {
    } else {
    }
  }

  onCellDoubleClicked($event: any): void {
    this.onViewCert($event.data);
  }
  //#endregion Ag-grid

  //#region Search

  //#endregion Search

  //#region Event

  onModalEventEmmit(event: any): void {
    this.modal.isShow = false;
    if (event.type === 'success') {
      this.initGridData();
    }
  }

  fillForm(item: any): void {
    // console.log(item);
    this.form.controls.commonName.setValue(item.commonName);
    this.form.controls.countryName.setValue(item.countryName);
    this.form.controls.email.setValue(item.email);
    this.form.controls.identityNumber.setValue(item.identityNumber);
    this.identityType = item.identityType;
    this.form.controls.phoneNumber.setValue(item.phoneNumber);
    this.form.controls.taxCode.setValue(item.taxCode);
    this.form.controls.organizationUnit.setValue(item.organizationUnit);
    this.form.controls.organizationName.setValue(item.organizationName);
    this.form.controls.address.setValue(item.address);

    this.form.controls.provinceName.setValue(this.listOfProvinces.find((c) => c.id === item.provinceId)?.name);
    this.form.controls.countryName.setValue(this.listOfCountries.find((c) => c.id === item.countryId)?.name);

    // User Id và OrgId nếu có
    this.user_id = item.userId;
    this.organization_id = item.organizationId;

    if (this.type === '3') {
    } else {
      this.form.controls.positionName.setValue(item.positionName);
    }
  }

  resetForm(): void {
    this.form.controls.agency.setValue(null);
    this.form.controls.tokenMethod.setValue(null);
    this.form.controls.caSupplier.setValue(null);
    this.form.controls.servicePack.setValue(null);
    this.form.controls.price.setValue(null);

    this.nationIDFileList = [];
    this.businessLicenseFileList = [];
    this.registrationFileList = [];
    this.confirmationFileList = [];
    this.isShowCertItemForm = false;

    this.initGridData();
  }

  onResetSearch(reloadData: boolean): void {
    this.filter.pageNumber = 1;
    this.filter.textSearch = undefined;
    this.filter.status = null;
    if (reloadData) {
      this.initGridData();
    }
  }

  onAddItem(): void {
    this.isShowCertItemForm = true;
  }

  onEditItem(item: any = null): void {}

  onViewCert(item: any = null): void {
    if (item === null) {
      const selectedRows = this.gridApi.getSelectedRows();
      item = selectedRows[0];
    }
    this.modal = {
      type: 'info',
      item,
      isShow: true,
      option: {},
    };
    this.viewCertModal.initData(item, 'view');
  }

  onDownloadCert(item: any = null): Subscription | undefined {
    const promise = this.certService.create(item.id).subscribe(
      (res: any) => {
        if (res.code !== 200) {
          this.messageService.error(`${res.message}`);
          return;
        }
        if (res.data === null || res.data === undefined) {
          this.messageService.error(`${res.message}`);
          return;
        }
        const dataResult = res.data;
        // const url = 'data:application/octet-stream;base64,' + dataResult.fileBase64;
        // const file = this.dataURLtoFile(url, dataResult.fileName);
        // console.log(file);

        const a = document.createElement('a');
        a.href = 'data:application/octet-stream;base64,' + dataResult.fileBase64;
        console.log(a.href);
        a.download = dataResult.fileName + '.cer';
        a.click();
      },
      (err: any) => {
        if (err.error) {
          this.messageService.error(`${err.error.message}`);
        } else {
          this.messageService.error(`${err.status}`);
        }
      },
    );
    return promise;
  }

  onCancel(): void {
    this.isShowCertItemForm = false;
  }

  onChangeServicePack(value: any): void {
    this.form.controls.price.setValue(this.listOfServicePack.find((c) => c.name === value)?.price);
    return;
  }

  handleChangeFile(info: NzUploadChangeParam, id: any): void {
    let fileList = [...info.fileList];
    // 1. Limit the number of uploaded files
    fileList = fileList.slice(-1);
    // 2. Read from response and show file link
    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        file.url = file.response.url;
      }
      return file;
    });

    if (id === 'nationIDFileList') {
      this.nationIDFileList = fileList;
    }
    if (id === 'businessLicenseFileList') {
      this.businessLicenseFileList = fileList;
    }
    if (id === 'registrationFileList') {
      this.registrationFileList = fileList;
    }
    if (id === 'confirmationFileList') {
      this.confirmationFileList = fileList;
    }
  }

  dataURLtoFile(dataurl: any, filename: any): File {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }

  initGridData(): Subscription {
    this.gridApi.showLoadingOverlay();
    const rs = this.certService.getFilter(this.filter).subscribe(
      (res: any) => {
        this.gridApi.hideOverlay();
        if (res.code !== 200) {
          this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
          return;
        }
        if (res.data === null || res.data === undefined) {
          this.notification.error(`Có lỗi xảy ra`, `${res.message}`);
          return;
        }

        const dataResult = res.data;

        let i =
          (this.filter.pageSize === undefined ? 0 : this.filter.pageSize) *
          ((this.filter.pageNumber === undefined ? 0 : this.filter.pageNumber) - 1);

        this.paginationMessage = `Hiển thị <b>${i + 1} - ${i + dataResult.dataCount}</b> trên tổng số <b>${
          dataResult.totalCount
        }</b> kết quả`;
        for (const item of dataResult.data) {
          item.index = ++i;
          item.downloadCertGrantAccess = true;
          item.infoGrantAccess = true;
        }
        this.grid.totalData = dataResult.totalCount;
        this.grid.dataCount = dataResult.dataCount;
        this.grid.rowData = dataResult.data;
        this.pageSizeOptions = [...PAGE_SIZE_OPTION_DEFAULT];
        // tslint:disable-next-line: variable-name
        this.pageSizeOptions = this.pageSizeOptions.filter((number) => {
          return number < dataResult.totalCount;
        });
        this.pageSizeOptions.push(dataResult.totalCount);
      },
      (err: any) => {
        this.gridApi.hideOverlay();
        // console.log(err);
      },
    );
    return rs;
  }
  //#endregion API Event

  save(isCreateAfter: boolean = false): Subscription | undefined {
    this.isLoading = true;
    cleanForm(this.form);
    // tslint:disable-next-line:forin
    for (const i in this.form.controls) {
      this.form.controls[i].markAsDirty();
      this.form.controls[i].updateValueAndValidity();
    }
    if (!this.form.valid) {
      this.isLoading = false;
      this.messageService.error(`Kiểm tra lại thông tin các trường đã nhập!`);
      return;
    }

    //#region File
    // Giấy tờ tùy thân
    let nationIDFileBucketName = null;
    let nationIDFileObjectName = null;
    let nationIDFileName = null;
    if (this.type !== '3') {
      if (this.nationIDFileList.length > 0 && this.type !== '3') {
        const nationFileResponse = this.nationIDFileList[0].response?.data;
        nationIDFileBucketName = nationFileResponse.bucketName ?? undefined;
        nationIDFileObjectName = nationFileResponse.fileName ?? undefined;
        nationIDFileName = nationFileResponse.objectName ?? undefined;
      } else {
        this.isLoading = false;
        this.messageService.error(`Chưa có giấy CMND`);
        return;
      }
    }

    // Giấy phép kinh doanh
    let businessLicensesFileBucketName = null;
    let businessLicensesFileObjectName = null;
    let businessLicensesFileName = null;
    if (this.type !== '1') {
      if (this.businessLicenseFileList.length > 0) {
        const businessFileResponse = this.businessLicenseFileList[0].response?.data;
        businessLicensesFileBucketName = businessFileResponse.bucketName ?? undefined;
        businessLicensesFileObjectName = businessFileResponse.fileName ?? undefined;
        businessLicensesFileName = businessFileResponse.objectName ?? undefined;
      } else {
        this.isLoading = false;
        this.messageService.error(`Chưa có giấy phép kinh doanh`);
        return;
      }
    }

    // Giấy đăng ký sử dụng dịch vụ
    let registrationFileBucketName = null;
    let registrationFileObjectName = null;
    let registrationFileName = null;

    if (this.registrationFileList.length > 0) {
      const registerFileResponse = this.registrationFileList[0].response?.data;
      registrationFileBucketName = registerFileResponse.bucketName ?? undefined;
      registrationFileObjectName = registerFileResponse.fileName ?? undefined;
      registrationFileName = registerFileResponse.objectName ?? undefined;
    } else {
      this.isLoading = false;
      this.messageService.error(`Chưa có giấy đăng ký sử dụng dịch vụ`);
      return;
    }

    // Giấy xác nhận
    let confirmationFileBucketName = null;
    let confirmationFileObjectName = null;
    let confirmationFileName = null;
    if (this.confirmationFileList.length > 0) {
      const confirmationFileResponse = this.confirmationFileList[0].response?.data;
      confirmationFileBucketName = confirmationFileResponse.bucketName ?? undefined;
      confirmationFileObjectName = confirmationFileResponse.fileName ?? undefined;
      confirmationFileName = confirmationFileResponse.objectName ?? undefined;
    } else {
      this.isLoading = false;
      this.messageService.error(`Chưa có giấy xác nhận`);
      return;
    }
    //#endregion

    const data = {
      commonName: this.form.controls.commonName.value,
      identityNumber: this.form.controls.identityNumber.value,
      taxCode: this.form.controls.taxCode.value,
      agency: this.form.controls.agency.value,
      certType: Number(this.type),
      userId: this.user_id,
      organizationId: this.organization_id,
      status: true,
      identityType: this.identityType,
      positionName: this.form.controls.positionName.value,
      organizationName: this.form.controls.organizationName.value,
      organizationUnit: this.form.controls.organizationUnit.value,
      email: this.form.controls.email.value,
      phoneNumber: this.form.controls.phoneNumber.value,
      address: this.form.controls.address.value,
      countryId: this.listOfCountries.find((c) => c.name === this.form.controls.countryName.value)?.id,
      countryCode: this.listOfCountries.find((c) => c.name === this.form.controls.countryName.value)?.code,
      countryName: this.form.controls.countryName.value,
      provinceId: this.listOfProvinces.find((c) => c.name === this.form.controls.provinceName.value)?.id,
      provinceCode: this.listOfProvinces.find((c) => c.name === this.form.controls.provinceName.value)?.code,
      provinceName: this.form.controls.provinceName.value,
      tokenMethod: this.form.controls.tokenMethod.value,
      servicePack: this.form.controls.servicePack.value,
      price: Number(this.form.controls.price.value),
      nationIDFileBucketName,
      nationIDFileObjectName,
      nationIDFileName,
      businessLicensesFileBucketName,
      businessLicensesFileObjectName,
      businessLicensesFileName,
      registrationFileBucketName,
      registrationFileObjectName,
      registrationFileName,
      confirmationFileBucketName,
      confirmationFileObjectName,
      confirmationFileName,
      order: 0,
      description: '',
    };

    const promise = this.certService.create(data).subscribe(
      (res: any) => {
        this.isLoading = false;
        if (res.code !== 200) {
          this.messageService.error(`${res.message}`);
          return;
        }
        if (res.data === null || res.data === undefined) {
          this.messageService.error(`${res.message}`);
          return;
        }
        const dataResult = res.data;
        this.resetForm();
        this.messageService.success(`${res.message}`);
      },
      (err: any) => {
        this.isLoading = false;
        if (err.error) {
          this.messageService.error(`${err.error.message}`);
        } else {
          this.messageService.error(`${err.status}`);
        }
      },
    );
    return promise;
  }
}

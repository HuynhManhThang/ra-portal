import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';

import { AgGridModule } from 'ag-grid-angular';
import { UserRoutingModule } from './user-routing.module';
import { NgZorroAntdModule } from 'src/app/shared/ng-zorror-ant/ng-zorro-antd.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserItemComponent } from './user-item/user-item.component';

// dashboard pages

const COMPONENTS: Type<void>[] = [UserListComponent, UserItemComponent];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), UserRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class UserModule {}

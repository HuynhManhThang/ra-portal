import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ACLService } from '@delon/acl';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { from, Subscription } from 'rxjs';

import { ChartJSModel } from '@model';
import { numberWithCommas } from '@util';
import * as moment from 'moment';

import { Router } from '@angular/router';
import { SCHEMA_THIRDS_COMPONENTS } from '@shared';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent implements OnInit {
  constructor(
    private aclService: ACLService,
    private notification: NzNotificationService,
    private elementRef: ElementRef,
    private router: Router,
  ) { }

  tittle = 'Trang chủ';

  cardHeader: any = {
    total1: 5000,
    total2: 13568,
    total3: 4650,
    total4: 25555,
  };

  extraTpl = 123;

  chart1: ChartJSModel = new ChartJSModel();
  chart2: ChartJSModel = new ChartJSModel();
  dataDashboard: any;
  caCount = {
    total: 0,
    active: 0,
    inactive: 0,
  };
  totalCertificateValidationCount = 0;

  dataCertificate: any;
  dataCertificateDetail: any;

  ngOnInit(): void {
    this.initRightOfUser();
    this.initDataDashboard();
    // this.initDataCertificate();
    // this.initDataCertificateDetail();
    this.chart1.data = [];
  }

  //#region ACL Service

  initRightOfUser(): void {
    // this.btnAdd.grandAccess = this.aclService.canAbility('UNIT-ADD');
    // this.btnDelete.grandAccess = this.aclService.canAbility('UNIT-DELETE');
    // this.btnExportExcel.grandAccess = this.aclService.canAbility('UNIT-EXPORT-EXCEL');
  }

  //#endregion ACL Service

  goToReport(): any {
    this.router.navigateByUrl('report');
  }

  //#region Dashboard
  initDataDashboard(): void {

  }

  //#endregion
  initChart1(): any {
    const caDetail: any[] = this.chart1.dataOrigin;

    this.chart1.data = [{ data: caDetail.map((x) => x.certificateAmount), label: 'Số lượng CTS đã được kiểm tra' }];

    this.chart1.labels = caDetail.map((x) => x.certificateAuthority);

    this.chart1.options = {
      responsive: true,
      // We use these empty structures as placeholders for dynamic theming.
      scales: { xAxes: [{}], yAxes: [{}] },
      plugins: {
        datalabels: {
          anchor: 'end',
          align: 'end',
        },
      },
      legend: {
        display: true,
        labels: {
          color: 'rgb(255, 99, 132)',
        },
        position: 'bottom',
      },
    };
    this.chart1.type = 'horizontalBar';
    this.chart1.ready = true;
  }
}

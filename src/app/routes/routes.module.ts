import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';


import { AgGridModule } from 'ag-grid-angular';

// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouteRoutingModule } from './routes-routing.module';

import { AgGridComponent } from './ag-grid/demo/ag-grid.component';
import { ConfirmComponent } from './passport/confirm/confirm.component';
import { LogoutComponent } from './passport/logout/logout.component';

import { ChartsModule } from 'ng2-charts';
import { NgZorroAntdModule } from '../shared/ng-zorror-ant/ng-zorro-antd.module';
import { HomePageComponent } from '../layout/home-page/home-page.component';


const COMPONENTS: Type<void>[] = [
  DashboardComponent,
  ConfirmComponent,
  LogoutComponent,
  // Ag-grid
  HomePageComponent,
  AgGridComponent,
];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), RouteRoutingModule, ChartsModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class RoutesModule { }

import { LayoutProComponent } from '@brand';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { CertificateListComponent } from './certificate-list/certificate-list.component';
import { CertificateItemComponent } from './certificate-item/certificate-item.component';
// layout

// dashboard pages

const routes: Routes = [
  { path: '', component: CertificateListComponent },
  { path: 'item', component: CertificateItemComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CertificateRoutingModule {}

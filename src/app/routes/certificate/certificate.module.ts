import { SharedModule } from '@shared';
import { NgModule, Type } from '@angular/core';

import { AgGridModule } from 'ag-grid-angular';
import { NgZorroAntdModule } from 'src/app/shared/ng-zorror-ant/ng-zorro-antd.module';
import { CertificateRoutingModule } from './certificate-routing.module';
import { CertificateListComponent } from './certificate-list/certificate-list.component';
import { CertificateItemComponent } from './certificate-item/certificate-item.component';

// dashboard pages

const COMPONENTS: Type<void>[] = [CertificateListComponent, CertificateItemComponent];
const COMPONENTS_NOROUNT: Type<void>[] = [];

@NgModule({
  imports: [SharedModule, NgZorroAntdModule, AgGridModule.withComponents([]), CertificateRoutingModule],
  declarations: [...COMPONENTS, ...COMPONENTS_NOROUNT],
  entryComponents: COMPONENTS_NOROUNT,
})
export class CertificateModule {}

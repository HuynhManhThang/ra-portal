import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DA_SERVICE_TOKEN, ITokenService } from '@delon/auth';
import { Wso2Service } from '@service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.less'],
})
export class ConfirmComponent implements OnInit {
  tokenExpired: any;
  isLoading = true;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    @Inject(DA_SERVICE_TOKEN) private tokenService: ITokenService,
    private wso2ApiService: Wso2Service,
    private notification: NzNotificationService,
  ) {
    this.activatedRoute.queryParams.subscribe((params) => {
      const authorization_code = params.code;
      console.log(authorization_code);
      localStorage.setItem('code', authorization_code);
      this.getAccessTokenWSO2(authorization_code);
    });
  }
  getAccessTokenWSO2(authorization_code: any): void {
    this.wso2ApiService.getAccessToken(authorization_code).subscribe(
      (res) => {
        this.isLoading = false;
        localStorage.setItem('data_token', JSON.stringify(res.data));
        if (res.data.userInfo !== null && res.data.userInfo !== undefined) {
          this.tokenExpired = false;
          const token = {
            appId: res.data.userInfo.app_id,
            // id: res.data.userInfo.id,
            rights: [],
            roles: [],
            token: res.data.access_token,
            name: res.data.userInfo.display_name,
            avatar: './assets/logo-color.svg',
          };
          this.tokenService.set(token);
          console.log('success');
          this.router.navigateByUrl('/dashboard');
        } else {
          this.tokenService.clear();
          this.tokenExpired = true;
          localStorage.removeItem('data_token');
          this.notification.error(`Expires_time: `, `${res.data.expires_time}`);
        }
      },
      (err) => {
        this.isLoading = false;
        console.log(err);
        this.notification.error(`Có lỗi xảy ra`, `${err.message}`);
      },
    );
  }
  ngOnInit(): void {}
  gotoLogin() {
    this.router.navigateByUrl('');
  }
}

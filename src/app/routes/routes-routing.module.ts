import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// layout
import { LayoutProComponent } from '@brand';
import { environment } from './../../environments/environment';
// dashboard pages
import { DashboardComponent } from './dashboard/dashboard.component';

import { JWTGuard } from '@delon/auth';
import { ConfirmComponent } from './passport/confirm/confirm.component';
import { LogoutComponent } from './passport/logout/logout.component';
import { CallBackCoppyService } from '../services/api/callback-coppy.service';
import { HomePageComponent } from '../layout/home-page/home-page.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutProComponent,
    canLoad: [JWTGuard],
    children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      // { path: 'ag-grid', component: AgGridComponent },
      {
        path: 'certificate',
        loadChildren: () => import('./certificate/certificate.module').then((m) => m.CertificateModule),
      },
      {
        path: 'agency',
        loadChildren: () => import('./agency/agency.module').then((m) => m.AgencyModule),
      },
      {
        path: 'payment',
        loadChildren: () => import('./payment/payment.module').then((m) => m.PaymentModule),
      },
      {
        path: 'user',
        loadChildren: () => import('./user/user.module').then((m) => m.UserModule),
      },
    ],
  }, // { path: '', redirectTo: 'register-account', pathMatch: 'full' },
  //21/08//2021

  {
    path: '',
    component: HomePageComponent,
    children: [
      {
        path: 'register-account',
        loadChildren: () => import('./register-account/register.module').then((m) => m.RegisterModule),
      },
    ],
  },

  {
    path: 'confirm',
    component: ConfirmComponent,
  },
  {
    path: 'logout',
    component: LogoutComponent,
  },
  // passport
  { path: '', loadChildren: () => import('./passport/passport.module').then((m) => m.PassportModule) },
  { path: '**', redirectTo: 'exception/404' },
  { path: 'exception', loadChildren: () => import('./exception/exception.module').then((m) => m.ExceptionModule) },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      useHash: environment.useHash,
      scrollPositionRestoration: 'top',
    }),
  ],
  exports: [RouterModule],
  providers: [CallBackCoppyService],
})
export class RouteRoutingModule {}

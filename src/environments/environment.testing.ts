// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  SERVER_URL: `./`,
  AUTH_TYPE: 'jwt',
  API_URL: 'https://uat-gateway.trustca.vn/singning-server/v1/',
  API_M_KEY:
    'eyJ4NXQiOiJNell4TW1Ga09HWXdNV0kwWldObU5EY3hOR1l3WW1NNFpUQTNNV0kyTkRBelpHUXpOR00wWkdSbE5qSmtPREZrWkRSaU9URmtNV0ZoTXpVMlpHVmxOZyIsImtpZCI6ImdhdGV3YXlfY2VydGlmaWNhdGVfYWxpYXMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJhZG1pbkBjYXJib24uc3VwZXIiLCJhcHBsaWNhdGlvbiI6eyJvd25lciI6ImFkbWluIiwidGllclF1b3RhVHlwZSI6bnVsbCwidGllciI6IlVubGltaXRlZCIsIm5hbWUiOiJTaWduaW5nU2VydmVfQXBwIiwiaWQiOjMwLCJ1dWlkIjoiNTNkZjBlMjUtNTgwZC00ZGRmLWJmN2EtZWYwMjFmZDFlNGI1In0sImlzcyI6Imh0dHBzOlwvXC91YXQtYXBpcG9ydGFsLnRydXN0Y2Eudm46NDQzXC9vYXV0aDJcL3Rva2VuIiwidGllckluZm8iOnsiVW5saW1pdGVkIjp7InRpZXJRdW90YVR5cGUiOiJyZXF1ZXN0Q291bnQiLCJzdG9wT25RdW90YVJlYWNoIjp0cnVlLCJzcGlrZUFycmVzdExpbWl0IjowLCJzcGlrZUFycmVzdFVuaXQiOm51bGx9fSwia2V5dHlwZSI6IlNBTkRCT1giLCJwZXJtaXR0ZWRSZWZlcmVyIjoiIiwic3Vic2NyaWJlZEFQSXMiOlt7InN1YnNjcmliZXJUZW5hbnREb21haW4iOiJjYXJib24uc3VwZXIiLCJuYW1lIjoiU2luZ25pbmdTZXJ2ZXIiLCJjb250ZXh0IjoiXC9zaW5nbmluZy1zZXJ2ZXJcL3YxIiwicHVibGlzaGVyIjoiYWRtaW4iLCJ2ZXJzaW9uIjoidjEiLCJzdWJzY3JpcHRpb25UaWVyIjoiVW5saW1pdGVkIn1dLCJwZXJtaXR0ZWRJUCI6IiIsImlhdCI6MTYyODE2MDg3NywianRpIjoiZTdiYzFiMTgtNTQ1Ny00MTBjLTg3MzctOGQzNmE5MTUxNjZiIn0=.DetoRO2gD6BUxM6usdOHqyRbXdtxfqjfdt31UiIPRwI2ami5jqLJwKjupuZ84Vq9cCN-IKX439NpMmyITllbyywmGXr8mgrKWbm7zm_fXuaBRTZBJH9bWqlTrMsDcEJDWgwf8srRFnXIjaYMHKGeQoIIu0sFPccWY_DPA1f4LnD6BW5ULAHlHcyKfz9j88VhO688XZuTxjNJdhygec-gML90AKctao5j8pfVWVBQ8vs87diFttXXMOg5COIz-AW3Bmq1Zjos5wMJSlOoQGuz-OuoowXcHshdKC0LOVnr7j7p4AMYIl6HexwRWT2fFpxPpfJea8JuZYwFIAL-_lR72Q==',

  BASE_WSO2_URL: 'https://uat-idp.digital-id.vn/',
  BASE_CALLBACK_URL: 'http://10.0.20.32:8220/confirm',
  BASE_LOGOUT_URL: 'http://10.0.20.32:8220/logout',
  APP_ID: '00000000-0000-0000-0000-000000000001',
  BASE_PUBLISH_WSO2_API: 'http://10.0.20.32:8271/api/v1/wso2/',
  CLIENT_ID: 'CNr2Ft4ATNINNVjgdRYioTA5XWMa',
  CLIENT_SECRET: '47M0fgmp7sGnZXxj3d9zR7rL4Dka',
  production: false,
  useHash: false,
  hmr: false,
  pro: {
    theme: 'light',
    menu: 'side',
    contentWidth: 'fluid',
    fixedHeader: true,
    autoHideHeader: true,
    fixSiderbar: true,
    onlyIcon: false,
    colorWeak: false,
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  SERVER_URL: `./`,
  AUTH_TYPE: 'jwt',
  API_URL: 'http://10.0.20.32:8271/',
  BASE_WSO2_URL: 'https://uat-idp.digital-id.vn/',
  BASE_CALLBACK_URL: 'http://10.0.20.32:8220/confirm',
  BASE_LOGOUT_URL: 'http://10.0.20.32:8220/logout',
  APP_ID: '00000000-0000-0000-0000-000000000001',
  BASE_PUBLISH_WSO2_API: 'http://10.0.20.32:8271/api/v1/wso2/',
  CLIENT_ID: 'CNr2Ft4ATNINNVjgdRYioTA5XWMa',
  CLIENT_SECRET: '47M0fgmp7sGnZXxj3d9zR7rL4Dka',
  production: false,
  useHash: false,
  hmr: false,
  pro: {
    theme: 'light',
    menu: 'side',
    contentWidth: 'fluid',
    fixedHeader: true,
    autoHideHeader: true,
    fixSiderbar: true,
    onlyIcon: false,
    colorWeak: false,
  },
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
